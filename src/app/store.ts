import {Action, configureStore, ThunkAction} from '@reduxjs/toolkit';
import {userSlice} from '../slices/userSlice';
import {loadState, saveState} from "./stogare";

const preloadedState = loadState();
export const store = configureStore({
    preloadedState,
    reducer: {
        user: userSlice.reducer,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;

store.subscribe(() => saveState(store.getState()));