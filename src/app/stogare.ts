const storedItems = ['user'];

export const saveState = (state: any) => {
    try {
        const storedData = storedItems.reduce((data, key: string) => {
            const result: { [key: string]: any } = {};
            result[key] = state[key];
            return { ...data, ...result };
        }, {});
        const serializedState = JSON.stringify(storedData);
        localStorage.setItem('state', serializedState);
    } catch (error) {
        console.error(error);
    }
};

export const loadState = () => {
    try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
            return undefined;
        }
        return JSON.parse(serializedState);
    } catch (error) {
        console.error(error);
        return undefined;
    }
};