import {AppThunk, RootState} from "../app/store";
import axios from "axios";
import {createSlice} from "@reduxjs/toolkit";

export interface UserState {
    username: string | null;
    token: string | null;
    pic: string | null;
    id: string | null;
    name: string | null;
    roles: string[] | null;
}

const initialState: UserState = {
    username: null,
    token: null,
    pic: null,
    id: null,
    name: null,
    roles: null,
};

const instance = axios.create({
    baseURL: 'http://localhost:8080'
});

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUser: (state, {payload: {username, pic, name, id, roles}}) => {
            return {
                ...state,
                username,
                pic,
                name,
                id,
                roles,
            };
        },
        setToken: (state, {payload}) => ({
            ...state,
            token: payload,
        }),
    },
});

export const getUserAction = (token: string): AppThunk => async dispatch => {
    const response = await instance('/api/v1/user', {
        method: 'GET',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: `Bearer_${token}`,
        },
    });

    dispatch(setUser(await response.data));
};

export const setTokenAction = (token: string): AppThunk => async dispatch => {
    dispatch(setToken(token));
};

export const logoutAction = (): AppThunk => async dispatch => {
    dispatch(setUser(initialState));
    dispatch(setToken(null));
};

export const {setUser, setToken} = userSlice.actions;

export const selectUserData = (state: RootState) => state.user;