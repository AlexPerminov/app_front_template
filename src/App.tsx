import React from 'react';
import './App.css';
import LoginPage from "./components/pages/LoginPage/LoginPage";
import {Switch, Route, useHistory, useParams, BrowserRouter} from 'react-router-dom';
import StartPage from "./components/pages/StartPage/StartPage";
import {AppStyled} from "./App.styled";

function App() {
    return (
        <AppStyled>
            <BrowserRouter>
                <Switch>
                    <Route exact path='/'>
                        <LoginPage/>
                    </Route>
                    <Route path='/google/:googleToken'>
                        <StartPage/>
                    </Route>
                    <Route path='/tests'>
                        <StartPage/>
                    </Route>
                </Switch>
            </BrowserRouter>
        </AppStyled>

    );
}

export default App;
