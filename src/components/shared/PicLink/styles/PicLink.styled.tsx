import styled from "styled-components";


export interface PicLinkProps {
    size: number;
}

export const Link = styled.a`
  text-decoration: none;
`;

export const Pic = styled.img<PicLinkProps>`
  width:  ${props => (props.size)}rem;
  height: ${props => (props.size)}rem;
`;
