import React, {FC} from 'react';
import styles from './PicLink.module.css';
import {Link, Pic} from "./styles/PicLink.styled";

export interface PicLinkProps {
    img: string;
    href?: string;
    size: number
}

const PicLink: FC<PicLinkProps> = (props: PicLinkProps) => (
    <Link href={props.href}>
        <Pic size={props.size} src={props.img}/>
    </Link>
);

export default PicLink;
