import React, {FC} from 'react';
import {Container, PageStyled} from "./styles/Page.styled";

interface PageProps {
}
export interface Pageable {
    offset: number;
    pageNumber: number;
    pageSize: number;
    paged: boolean;
    sort: Object;
    unpaged: boolean;
}

export interface PageView {
    content: [];
    pageable: Pageable;
    sort: Object;
    totalElements: number;
    totalPages: number;
    size: number;
    number: number;
    first: boolean;
    last: boolean;
}

const Page: FC<PageProps> = ({ children }) => {
    return (
        <PageStyled>
            <Container>
                { children }
            </Container>
        </PageStyled>
    );
};

export default Page;
