import styled from "styled-components";

export const mainColor: string = '#555555';
export const inputColor: string = '#656565';
export const mainPink: string = '#A79AE0';
export const mainBlue: string = '#2fc9ea';

export const PageStyled = styled.div`
  //font-family: Roboto, sans-serif;
  //font-weight: lighter;
  //@import url('https://fonts.googleapis.com/css2?family=Albert+Sans:ital,wght@0,400;0,500;0,700;1,300&display=swap');
  //font-family: 'Albert Sans', sans-serif;
  @import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');
  font-family: "Montserrat", sans-serif;
  width: 100%;
  background-color: ${mainColor};
  min-height: 100vh;
  display: flex;
`;

export const Container = styled.div`
  width: 70%;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
`;