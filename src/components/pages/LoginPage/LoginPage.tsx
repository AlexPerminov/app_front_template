import React, {FC, useEffect} from 'react';
import {LoginContainer, LoginStyled, LoginTitle} from "./styles/LoginPage.styled";
import Page from "../../shared/Page/Page";
import PicLink from "../../shared/PicLink/PicLink";
import GoogleLogo from '../../../pictures/google_logo.png';
import {useDispatch, useSelector} from "react-redux";
import {getUserAction, logoutAction, selectUserData} from "../../../slices/userSlice";

interface LoginPageProps {
}

const LoginPage: FC<LoginPageProps> = () => {
        const dispatch = useDispatch();
        const {id} = useSelector(selectUserData);
        useEffect(() => {
            if (id != null) {
                dispatch(logoutAction())
            }
        })
        return (
            <Page>
                <LoginContainer>
                    <LoginStyled>
                        <LoginTitle>SIGN IN</LoginTitle>
                        <PicLink size={4} img={GoogleLogo} href={'http://localhost:8080/oauth2/authorization/google'}/>
                    </LoginStyled>
                </LoginContainer>
            </Page>

        );
    }
;

export default LoginPage;
