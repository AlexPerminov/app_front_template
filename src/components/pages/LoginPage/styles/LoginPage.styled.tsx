import styled from 'styled-components';

export const LoginContainer = styled.div`
  width: 90%;
  display: flex;
  flex-direction: column;
  flex-grow: 1;
  justify-content: center;
  align-items: center;
`;
export const LoginStyled = styled.div`
  padding: 1rem;
  display: flex;
  width: 35%;
  justify-content: space-between;
  align-items: center;
`;

export const LoginTitle = styled.div`
  color: #FFFFFF;
  font-size: 3rem;
`;
