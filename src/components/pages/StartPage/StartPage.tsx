import React, {FC, useEffect} from 'react';
import Page from "../../shared/Page/Page";
import {useHistory, useParams} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';
import {getUserAction, selectUserData, setToken} from "../../../slices/userSlice";
import UserInfo from "../../UserInfo/UserInfo";
import PicLink from "../../shared/PicLink/PicLink";
import Logout from "../../../pictures/log_out.png"

interface MainPageProps {
}

const StartPage: FC<MainPageProps> = () => {

    const {googleToken}: { googleToken: string } = useParams();
    const dispatch = useDispatch();
    const history = useHistory();
    const {token, id, username} = useSelector(selectUserData);

    useEffect(() => {
        if (googleToken) {
            dispatch(setToken(googleToken))
        }
    }, [])

    useEffect(() => {
        if (token && id === null) {
            dispatch(getUserAction(googleToken));
            history.replace('/tests')
        }
    }, [token])

    return (
        <>
            <Page>
                Hello, {username}!
                <UserInfo/> <PicLink img={Logout} size={2} href={'/'}/>
            </Page>
        </>

    )
};

export default StartPage;
