import styled from "styled-components";

export const UserInfoContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const UserPic = styled.img`
  max-width: 3.5rem;
  max-height: 3.5rem;
  margin-right: 1rem;
  border-radius: 45px;
`;

export const UserDataContainer = styled.div`
`;

export const UserInfoLine = styled.div`
  color: #ffffff;
`;