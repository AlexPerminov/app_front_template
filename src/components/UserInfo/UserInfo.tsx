import React, {FC} from "react";
import {useSelector} from "react-redux";
import {selectUserData} from "../../slices/userSlice";
import {UserDataContainer, UserInfoContainer, UserInfoLine, UserPic} from "./UserInfo.styled";

const UserInfo: FC = () => {

    const UserInfo = useSelector(selectUserData);

    return (
        <UserInfoContainer>
            <UserPic src={UserInfo.pic ? UserInfo.pic : ''}/>
            <UserDataContainer>
                <UserInfoLine>{UserInfo.name}</UserInfoLine>
                <UserInfoLine>{UserInfo.username}</UserInfoLine>
            </UserDataContainer>
        </UserInfoContainer>
    )
};

export default UserInfo;